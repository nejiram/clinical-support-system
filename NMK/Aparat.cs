﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;

namespace NMK
{
    public class Aparat
    {
        private string nazivAparata;
        int uKojojJeOrdinaciji;
        bool radi;

        public Aparat(string nazivAparata, int uKojojJeOrdinaciji, bool radi)
        {
            this.nazivAparata = nazivAparata;
            this.uKojojJeOrdinaciji = uKojojJeOrdinaciji;
            this.radi = radi;
        }

        public Aparat()
        {
            this.nazivAparata= "";
            this.uKojojJeOrdinaciji = 0;
            this.radi = false;
        }

        public string NazivAparata
        {
            get
            {
                return nazivAparata;
            }
            set
            {
                nazivAparata = value;
            }
        }

        public int UKojojJeOrdinaciji
        {
            get
            {
                return uKojojJeOrdinaciji;
            }
            set
            {
                Validacija.validacija v = new Validacija.validacija();
                if (!v.BrOrdinacije()) throw new System.ArgumentException("Ordinacije su numerirane brojevima od 1 do 11!");
                uKojojJeOrdinaciji = value;
            }
        }

        public bool Radi
        {
            get
            {
                return radi;
            }
            set
            {
                radi = value;
            }
        }

        public string ispisiAparat()
        {
            string ispis;
            ispis=("Aparat: " + nazivAparata.ToString());
            if (!radi) ispis+="\nAparat u kvaru";
            return ispis;
        }

        public string DajNazivAparata()
        {
            return nazivAparata;
        }

        public int DajUKojojJeOrdinaciji()
        {
            return uKojojJeOrdinaciji;
        }

        public bool DaLiRadi()
        {
            return radi;
        }

        public string IspisiAparat()
        {
            string ispis;
            ispis = ("Naziv aparat: " + nazivAparata.ToString() +
                "\n" + "Broj ordinacije: " + uKojojJeOrdinaciji.ToString() + "\n"
                + "Ispravan: " + radi.ToString() + "\n");
            return ispis;
        }

    }
}
