﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;


namespace NMK
{
    public class Doktor
    {
        private bool zauzet, uOrdinaciji;
        private string ime, prezime, user, password;
        private int brojOrdinacije;
    

        public Doktor(string ime, string prezime, int brojOrdinacije, bool zauzet, bool uOrdinaciji, string user, string password)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.brojOrdinacije = brojOrdinacije;
            this.zauzet = zauzet;
            this.uOrdinaciji = uOrdinaciji;
            this.user = user;
            this.password = password;
        }

        public Doktor() { }

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Ime()) throw new System.ArgumentException("Ime smije sadržavati samo slova!");
                ime = value;
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Prezime()) throw new System.ArgumentException("Prezime smije sadržavati samo slova!");
                prezime = value;
            }
        }

        public int BrojOrdinacije
        {
            get
            {
                return brojOrdinacije;
            }
            set
            {
                Validacija.validacija v = new Validacija.validacija();
                if (!v.BrOrdinacije()) throw new System.ArgumentException("Ordinacije su numerirane brojevima od 1 do 11!");
                brojOrdinacije = value;
            }
        }

        public bool UOrdinaciji
        {
            get
            {
                return uOrdinaciji;
            }
            set
            {
                uOrdinaciji = value;
            }
        }

        public bool Zauzet
        {
            get
            {
                return zauzet;
            }
            set
            {
                zauzet = value;
            }
        }

        public string DajImeIPrezime()
        {
            return ime + " "+prezime;
        }

        public int DajBrojOrdinacije()
        {
            return brojOrdinacije;
        }

        public bool DaLiJeZauzet()
        {
            return zauzet;
        }

        public bool DaLiJeUOrdinaciji()
        {
            return uOrdinaciji;
        }

        public string DajUser()
        {
            return user;
        }

        public string DajPassword()
        {
            return password;
        }

        public string User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public string IspisiDoktora()
        {
            string ispis;
            ispis = ("Ime i prezime doktora: " + ime + " " + prezime + "\n" +
                "Broj ordinacije: " + brojOrdinacije + "\n" +
                "Da li je u ordinaciji: " + uOrdinaciji + "\n" +
                "Da li je zauzet: " + zauzet + "\n");
            return ispis;
        }

        static string GetMd5Hash(MD5 md5Hash, string password)
        {

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
