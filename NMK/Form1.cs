﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form1 : Form
    {
        
        Klinika k;
        Ordinacije o;
        System.Type tip;
        string put;

        public string Put
        {
            get
            {
                return put;
            }
            set
            {
                put = value;
            }
        }
        public System.Type Tip
        {
            get
            {
                return tip;
            }
            set
            {
                tip = value;
            }
        }



        public Form1()
        {
            InitializeComponent();
            k = new Klinika();

        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '\0';
        }

        private void button2_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = "";
            if (textBox2.Text != textBox3.Text)
            {
                //toolStripStatusLabel1.Text = "Razlika u siframa!";
                errorProvider1.SetError(textBox3,"Razlika u siframa!");
            }
            else
            {
                string user = textBox1.Text;
                string pw = textBox2.Text;

                foreach (Doktor d in k.DajDoktore())
                {
                    if (d.User == user && pw == d.Password)
                    {
                        Form2 f = new Form2(k);
                        f.Show();
                        this.Hide();
                    }
                }

                foreach (Portir p in k.DajPortire())
                {
                    if (p.User == user && pw == p.Password)
                    {
                        Form3 f = new Form3(k);
                        f.Show();
                        this.Hide();
                    }
                }

                foreach (MedicinskiTehnicar m in k.DajMedTehnicare())
                {
                    if (m.User == user && pw == m.Password)
                    {
                        Form4 f = new Form4(k);
                        f.Show();
                        this.Hide();
                    }
                }

                foreach(Pacijent p in k.DajPacijente())
                {
                    if(p.User==user && pw == p.Password)
                    {
                        Form5 f = new Form5(k);
                        f.Show();
                        this.Hide();
                    }
                }

                toolStripStatusLabel2.Text = "Pogresni pristupni podaci!";
                toolStripStatusLabel2.Visible = true;
            }
            
            
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Paint += Form1_Paint;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
             e.Graphics.DrawEllipse(Pens.Navy, new Rectangle(30, 30, 180, 80));
            e.Graphics.DrawString("Dobro došli na\n       NMK", new Font("Verdana", 15), Brushes.Navy, 50, 50);
            e.Graphics.DrawLine(Pens.Magenta, 50, 73, 195, 73);
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }
    }
}
