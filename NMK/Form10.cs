﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form10 : Form
    {
        Klinika k;
        private string uzrokSmrti, prvaPomoc;
        private DateTime vrijemeSmrti, vrijemeObdukcije;

        public Form10()
        {
            InitializeComponent();
        }

        public Form10(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            prvaPomoc = textBox1.Text;
            uzrokSmrti = textBox2.Text;
            vrijemeSmrti = dateTimePicker1.Value;
            vrijemeObdukcije = dateTimePicker2.Value;

            Form9 f = new Form9(k);
            f.Show();
            this.Hide();
        }
    }
}
