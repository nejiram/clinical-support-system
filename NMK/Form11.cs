﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form11 : Form
    {
        Klinika k;
        private string jmbg;
        private int brOrd;
        DateTime datumRodjenja;

        public Form11()
        {
            InitializeComponent();
        }

        public Form11(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            jmbg = textBox1.Text;
            if (!validacijaJMBG(jmbg)) errorProvider1.SetError(textBox1, "Pogrešan unos");
            Int32.TryParse(comboBox1.Text, out brOrd);
            k.ZakaziPregled(jmbg, brOrd);
            foreach(Pacijent p in k.DajPacijente())
            {
                if (jmbg == p.DajMaticniBroj())
                {
                    p.PovecajBrojPregleda();
                }
            }
            MessageBox.Show("Pregled uspješno zakazan!");
        }

        bool validacijaJMBG(string JMBG)
        {
            
            foreach (Pacijent p in k.DajPacijente())
            {
                if (JMBG == p.DajMaticniBroj())
                {
                    datumRodjenja = p.DajDatumRodjenja();
                    break;
                }
            }

            if (JMBG.Length != 13) return false;
            long broj;
            //da li ima slova
            bool proba = long.TryParse(JMBG, out broj);
            if (proba == false) return false;
            //da li odg prvih par cifara 
            string pom = Convert.ToString(datumRodjenja);

            string pom2;

            if (pom[4] != '.')
            {

                if (pom[3] == '.' && pom[1] == '.')
                {

                    pom2 = "0" + Convert.ToString(pom[0]) + "0" + Convert.ToString(pom[2]) + Convert.ToString(pom[5]) + Convert.ToString(pom[6]) + Convert.ToString(pom[7]);

                }
                else if (pom[1] == '.' && pom[3] != '.')
                {

                    pom2 = "0" + Convert.ToString(pom[0]) + Convert.ToString(pom[2]) + Convert.ToString(pom[3]) + Convert.ToString(pom[6]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]);
                }
                else
                {

                    pom2 = Convert.ToString(pom[0]) + Convert.ToString(pom[1]) + Convert.ToString(pom[3]) +
                    Convert.ToString(pom[4]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]) + Convert.ToString(pom[9]);
                }


            }

            else
            {

                pom2 = Convert.ToString(pom[0]) + Convert.ToString(pom[1]) + "0" + Convert.ToString(pom[3]) +
                   Convert.ToString(pom[6]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]);
            }

            pom = Convert.ToString(JMBG[0]) + Convert.ToString(JMBG[1]) + Convert.ToString(JMBG[2]) + Convert.ToString(JMBG[3]) + Convert.ToString(JMBG[4]) + Convert.ToString(JMBG[5]) + Convert.ToString(JMBG[6]);

            if (pom != pom2) return false;

            string pom3 = JMBG;
            int cifra = Convert.ToInt32(pom3[pom3.Length - 1]) - 48;
            int suma = 11 - ((7 * (Convert.ToInt32(pom3[0]) + Convert.ToInt32(pom3[6]) - 96) + 6 * (Convert.ToInt32(pom3[1])
                 + Convert.ToInt32(pom3[7]) - 96) + 5 * (Convert.ToInt32(pom3[2]) + Convert.ToInt32(pom3[8]) - 96) +
                 4 * (Convert.ToInt32(pom3[3]) + Convert.ToInt32(pom3[9]) - 96) + 3 * (Convert.ToInt32(pom3[4]) +
                 Convert.ToInt32(pom3[10]) - 96) + 2 * (Convert.ToInt32(pom3[5]) + Convert.ToInt32(pom3[11]) - 96)) % 11);

            if (suma != cifra) return false;


            return true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form4 f = new Form4(k);
            f.Show();
            this.Hide();
        }

        private void Form11_Load(object sender, EventArgs e)
        {

        }
    }
}
