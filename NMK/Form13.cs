﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form13 : Form
    {
        Klinika k;
        string jmbg;

        public Form13(Klinika k, string jmbg)
        {
            InitializeComponent();
            this.k = k;
            this.jmbg = jmbg;
        }

        public Form13()
        {
            InitializeComponent();
        }

        private void Form13_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2(k);
            f.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string anamneza, dijagnoza, alergije, terapija;
            anamneza = textBox1.Text;
            alergije = textBox2.Text;
            dijagnoza = textBox3.Text;
            terapija = textBox4.Text;
            if (anamneza != "")
            
                foreach(Pacijent p in k.DajPacijente())
                {
                    if (jmbg == p.DajMaticniBroj())
                    {
                        if (anamneza != "")  p.AzurirajAnamnezu(anamneza);
                        if (alergije != "") p.AzurirajAlergije(alergije);
                        if (dijagnoza != "") p.AzurirajDijagnozu(dijagnoza);
                        if (terapija != "") p.AzurirajTerapiju(terapija);
                    }
                }

            MessageBox.Show("Ažuriranje uspješno!");
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
