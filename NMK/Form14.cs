﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form14 : Form
    {
        Klinika k;
        string jmbg;


        public Form14()
        {
            InitializeComponent();
        }

        public Form14(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void Form14_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            jmbg = textBox2.Text;
            foreach (Pacijent p in k.DajPacijente())
            {
                if (jmbg == p.MaticniBroj)
                {
                    Form13 f = new Form13(k, jmbg);
                    f.Show();
                    this.Hide();

                }
                else
                {
                    errorProvider2.SetError(textBox1, "Pogrešan JMBG!");
                }
                
            }
        }







    }

    }