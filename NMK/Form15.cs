﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form15 : Form
    {
        Klinika k;
        string jmbg;

        public Form15()
        {
            InitializeComponent();
        }

        public Form15(Klinika k, string jmbg)
        {
            InitializeComponent();
            this.k = k;
            this.jmbg = jmbg;
        }

        private void Form15_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            jmbg = textBox2.Text;
            foreach (Pacijent p in k.DajPacijente())
            {
                if (jmbg == p.MaticniBroj)
                {
                    Form16 f = new Form16(k, jmbg);
                    f.Show();
                    this.Hide();

                }
                else
                {
                    errorProvider1.SetError(textBox2, "Pogrešan JMBG!");
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
