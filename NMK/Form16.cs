﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form16 : Form
    {
        Klinika k;
        int racun;
        string jmbg;

        public Form16()
        {
            InitializeComponent();
        }

        public Form16(Klinika k, string jmbg)
        {
            InitializeComponent();
            this.k = k;
            this.jmbg = jmbg;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3(k);
            f.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pom;
            pom = textBox1.Text;
            Int32.TryParse(pom, out racun);

            foreach (Pacijent p in k.DajPacijente())
            {
                if (jmbg == p.MaticniBroj)
                {
                    
                    p.IzvrsiNaplatu(racun);
                    MessageBox.Show("Naplata uspještno izvršena!");

                }
                
            }
        }
    }
}
