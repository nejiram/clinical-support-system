﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form17 : Form
    {
        Klinika k;
        string jmbg;

        public Form17()
        {
            InitializeComponent();
        }

        public Form17(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            jmbg = textBox2.Text;
            foreach (Pacijent p in k.DajPacijente())
            {
                if (jmbg == p.MaticniBroj)
                {
                    int racun;
                    racun = p.ZaPlatiti();
                    MessageBox.Show(racun.ToString());
                }
                else
                {
                    errorProvider1.SetError(textBox2, "Pogrešan JMBG!");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form4 f = new Form4(k);
            f.Show();
            this.Hide();
        }
    }
}
