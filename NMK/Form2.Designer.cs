﻿namespace NMK
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opcijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledKartonaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosUKartonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakažiPregledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otkažiPregledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prikažiStatistikuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(56, 423);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(218, 31);
            this.button3.TabIndex = 2;
            this.button3.Text = "Odjava";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(38, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Prijavljeni ste kao doktor!";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcijeToolStripMenuItem,
            this.statistikaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(348, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // opcijeToolStripMenuItem
            // 
            this.opcijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledKartonaToolStripMenuItem,
            this.unosUKartonToolStripMenuItem,
            this.preglediToolStripMenuItem});
            this.opcijeToolStripMenuItem.Name = "opcijeToolStripMenuItem";
            this.opcijeToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.opcijeToolStripMenuItem.Text = "Opcije";
            // 
            // pregledKartonaToolStripMenuItem
            // 
            this.pregledKartonaToolStripMenuItem.Name = "pregledKartonaToolStripMenuItem";
            this.pregledKartonaToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.pregledKartonaToolStripMenuItem.Text = "Pregled kartona";
            this.pregledKartonaToolStripMenuItem.Click += new System.EventHandler(this.pregledKartonaToolStripMenuItem_Click);
            // 
            // unosUKartonToolStripMenuItem
            // 
            this.unosUKartonToolStripMenuItem.Name = "unosUKartonToolStripMenuItem";
            this.unosUKartonToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.unosUKartonToolStripMenuItem.Text = "Uredi karton";
            this.unosUKartonToolStripMenuItem.Click += new System.EventHandler(this.unosUKartonToolStripMenuItem_Click);
            // 
            // preglediToolStripMenuItem
            // 
            this.preglediToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zakažiPregledToolStripMenuItem,
            this.otkažiPregledToolStripMenuItem});
            this.preglediToolStripMenuItem.Name = "preglediToolStripMenuItem";
            this.preglediToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.preglediToolStripMenuItem.Text = "Pregledi";
            this.preglediToolStripMenuItem.Click += new System.EventHandler(this.preglediToolStripMenuItem_Click);
            // 
            // zakažiPregledToolStripMenuItem
            // 
            this.zakažiPregledToolStripMenuItem.Name = "zakažiPregledToolStripMenuItem";
            this.zakažiPregledToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.zakažiPregledToolStripMenuItem.Text = "Zakaži pregled";
            this.zakažiPregledToolStripMenuItem.Click += new System.EventHandler(this.zakažiPregledToolStripMenuItem_Click);
            // 
            // otkažiPregledToolStripMenuItem
            // 
            this.otkažiPregledToolStripMenuItem.Name = "otkažiPregledToolStripMenuItem";
            this.otkažiPregledToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.otkažiPregledToolStripMenuItem.Text = "Otkaži pregled";
            this.otkažiPregledToolStripMenuItem.Click += new System.EventHandler(this.otkažiPregledToolStripMenuItem_Click);
            // 
            // statistikaToolStripMenuItem
            // 
            this.statistikaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prikažiStatistikuToolStripMenuItem});
            this.statistikaToolStripMenuItem.Name = "statistikaToolStripMenuItem";
            this.statistikaToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.statistikaToolStripMenuItem.Text = "Statistika";
            this.statistikaToolStripMenuItem.Click += new System.EventHandler(this.statistikaToolStripMenuItem_Click);
            // 
            // prikažiStatistikuToolStripMenuItem
            // 
            this.prikažiStatistikuToolStripMenuItem.Name = "prikažiStatistikuToolStripMenuItem";
            this.prikažiStatistikuToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.prikažiStatistikuToolStripMenuItem.Text = "Prikaži statistiku";
            this.prikažiStatistikuToolStripMenuItem.Click += new System.EventHandler(this.prikažiStatistikuToolStripMenuItem_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(348, 519);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.Navy;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Doktor";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledKartonaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosUKartonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistikaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakažiPregledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otkažiPregledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prikažiStatistikuToolStripMenuItem;
    }
}