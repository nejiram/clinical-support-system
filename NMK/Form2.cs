﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form2 : Form
    {
        private string ime, prezime, pKarton, jmbg, spol, adresaStanovanja, bracnoStanje, uzrokSmrti, anamneza, dijagnoza, alergije, terapija;

        private void otkažiPregledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form12 f = new Form12(k);
            f.Show();
            this.Hide();
        }

        private void unosUKartonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form14 f = new Form14(k);
            f.Show();
            this.Hide();
        }

        private void statistikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void zakažiPregledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form10 f = new Form10(k);
            f.Show();
            this.Hide();
        }



        private void prikažiStatistikuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void preglediToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private DateTime datumRodjenja, vrijemeSmrti, vrijemeObdukcije;
        private int uReduCekanja, plati, brPregleda;
        Klinika k;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void pregledKartonaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 f = new Form5(k);
            f.Show();
            this.Hide();
        }
    }
}
