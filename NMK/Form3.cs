﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form3 : Form
    {
        Klinika k;

        public Form3()
        {
            InitializeComponent();
        }

        public Form3(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void provjeriStanjeOrdinacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 f = new Form7(k);
            f.Show();
            this.Hide();
        }

        private void prikažiStatistikuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void statistikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void izvršiNaplatuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
