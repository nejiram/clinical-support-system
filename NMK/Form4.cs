﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form4 : Form
    {
        Klinika k;

        public Form4()
        {
            InitializeComponent();
        }

        public Form4(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void prikažiKartonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 f = new Form5(k);
            f.Show();
            this.Hide();
        }

        private void provjeriStanjeOrdinacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 f = new Form7(k);
            f.Show();
            this.Hide();
        }

        private void prikažiStatistikuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void noviPacijentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form9 f = new Form9(k);
            f.Show();
            this.Hide();
        }

        private void noviHitanSlučajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form10 f = new Form10(k);
            f.Show();
            this.Hide();
        }

        private void zakažiPregledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form10 f = new Form10(k);
            f.Show();
            this.Hide();
        }

        private void otkažiPregledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form12 f = new Form12(k);
            f.Show();
            this.Hide();
        }

        private void statistikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 f = new Form8(k);
            f.Show();
            this.Hide();
        }

        private void izdajRačunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form17 f = new Form17(k);
            f.Show();
            this.Hide();
        }
    }
}
