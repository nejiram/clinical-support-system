﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form5 : Form
    {
        Klinika k;

        public Form5()
        {
            InitializeComponent();
            
        }

        public Form5(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string jmbg = textBox1.Text;
            if (k.BrojPacijenata() != 0)
            {
                foreach (Pacijent p in k.DajPacijente())
                {
                    if (jmbg == p.MaticniBroj)
                    {
                        Form6 f = new Form6(jmbg,k);
                        f.Show();
                        this.Hide();

                    }
                    else
                    {
                        errorProvider3.SetError(textBox1, "Pogrešan JMBG!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Nema registrovanih pacijenata.");
            }
            
        }
    }
}
