﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form6 : Form
    {
        private string ime, prezime, pKarton, jmbg, spol, adresaStanovanja, bracnoStanje, uzrokSmrti, anamneza, dijagnoza, alergije, terapija;

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private DateTime datumRodjenja, vrijemeSmrti, vrijemeObdukcije;
        private int uReduCekanja, plati, brPregleda;
        Klinika k;

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void label15_Click(object sender, EventArgs e)
        {

        }

        


        public Form6(string jmbg,Klinika k)
        {
            InitializeComponent();
            this.jmbg = jmbg;
            this.k = k;
        }

        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            foreach(Pacijent p in k.DajPacijente())
            {
                if (p.MaticniBroj == jmbg)
                {
                    ime = p.Ime;
                    prezime = p.Prezime;
                    datumRodjenja = p.DatumRodjenja;
                    bracnoStanje = p.BracnoStanje;
                    adresaStanovanja = p.AdresaStanovanja;
                    spol = p.Spol;
                    pKarton = p.DajBrojKartona();
                    anamneza = p.DajAnamnezu();
                    dijagnoza = p.DajDijagnozu();
                    alergije = p.DajAlergije();
                    terapija = p.DajTerapiju();
                    vrijemeSmrti = p.VrijemeSmrti;
                    vrijemeObdukcije = p.VrijemeObdukcije;
                    uReduCekanja = p.KojiJeUReduCekanja();
                    uzrokSmrti = p.DajUzrokSmrti();
                    plati = p.ZaPlatiti();

                    textBox1.Text = ime;
                    textBox2.Text = prezime;
                    dateTimePicker1.Value = datumRodjenja;
                    textBox3.Text = spol;
                    textBox4.Text = adresaStanovanja;
                    textBox5.Text = bracnoStanje;
                    textBox6.Text = pKarton;
                    textBox7.Text = anamneza;
                    textBox8.Text = alergije;
                    textBox9.Text = dijagnoza;
                    textBox10.Text = terapija;
                    dateTimePicker2.Value = vrijemeSmrti;
                    dateTimePicker3.Value = vrijemeObdukcije;
                    textBox11.Text = uzrokSmrti;
                    textBox13.Text = plati.ToString();
                    textBox12.Text = uReduCekanja.ToString();

                    if (uzrokSmrti != "")
                    {
                        label12.Visible = true;
                        label13.Visible = true;
                        label15.Visible = false;
                        label14.Visible = true;
                        textBox11.Visible = true;
                        dateTimePicker2.Visible = true;
                        dateTimePicker3.Visible = true;
                        textBox12.Visible = false;
                    }
                }
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Close();
        }
    }
}
