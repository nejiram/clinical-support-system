﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form7 : Form
    {
        Klinika k;

        public Form7()
        {
            InitializeComponent();
        }

        public Form7(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int brOrd;
            Int32.TryParse(textBox1.Text, out brOrd);
            if(brOrd<1 || brOrd > 11)
            {
                MessageBox.Show("Pogrešan unos");
            }
            else
            {
                int pom;
                pom = k.DajStanjeOrd(brOrd);

                MessageBox.Show(pom.ToString());
            }
        }
    }
}
