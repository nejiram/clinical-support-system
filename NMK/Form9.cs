﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NMK
{
    public partial class Form9 : Form
    {
        Klinika k;
        private string ime, prezime, user, password,pKarton, jmbg, spol, adresaStanovanja, bracnoStanje, anamneza, dijagnoza, alergije, terapija;
        private DateTime datumRodjenja, datumPregleda;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form4 f = new Form4(k);
            f.Show();
            this.Hide();
        }

        private int uReduCekanja, plati, brPregleda;
        private string uzrokSmrti, prvaPomoc;
        private DateTime vrijemeSmrti, vrijemeObdukcije;

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        private Form9(string prvaPomoc, string uzrokSmrti, DateTime vrijemeSmrti, DateTime vrijemeObdukcije)
        {
            this.prvaPomoc = prvaPomoc;
            this.uzrokSmrti = uzrokSmrti;
            this.vrijemeSmrti = vrijemeSmrti;
            this.vrijemeObdukcije = vrijemeObdukcije;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ime = textBox1.Text;
            prezime = textBox2.Text;
            datumRodjenja = dateTimePicker1.Value;
            jmbg = textBox3.Text;
            if (!validacijaJMBG(jmbg)) errorProvider1.SetError(textBox3, "Pogrešan unos");
            if (radioButton1.Checked) spol = "žensko";
            else if (radioButton2.Checked) spol = "muško";
            adresaStanovanja = textBox5.Text;
            bracnoStanje = textBox6.Text;
            pKarton = textBox7.Text;
            foreach(Pacijent a in k.DajPacijente()){
                if (pKarton == a.DajBrojKartona())
                {
                    errorProvider3.SetError(textBox7, "Već postoji pacijent sa unesenim brojem kartona");
                }
            }
            user = textBox8.Text;
            if (textBox9.Text != textBox10.Text) errorProvider2.SetError(textBox10, "Razlika u šiframa");
            else password = textBox9.Text;
            datumPregleda = dateTimePicker2.Value;
            int brOrd;
            Int32.TryParse(comboBox1.Text, out brOrd);
            k.ZakaziPregled(jmbg, brOrd);
            Karton brojKartona = new Karton(pKarton);

            if (uzrokSmrti != "")
            {
                HitanPacijent hp = new HitanPacijent(ime, prezime, datumRodjenja, jmbg, spol, adresaStanovanja, bracnoStanje, datumPregleda, prvaPomoc, uzrokSmrti, vrijemeSmrti, vrijemeObdukcije, brojKartona, user, password);

                k.DodajPacijenta(hp);
                MessageBox.Show("Pacijent uspjesno registriran!");
            }
            else
            {
                Pacijent p = new Pacijent(ime, prezime, datumRodjenja, jmbg, spol, adresaStanovanja, bracnoStanje, datumPregleda, brojKartona, user, password);
                k.DodajPacijenta(p);
                MessageBox.Show("Pacijent uspjesno registriran!");
            }

        }

        

        public Form9()
        {
            InitializeComponent();
        }

        public Form9(Klinika k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form9_Load(object sender, EventArgs e)
        {

        }

        bool validacijaJMBG(string JMBG)
        {


            if (JMBG.Length != 13) return false;
            long broj;
            //da li ima slova
            bool proba = long.TryParse(JMBG, out broj);
            if (proba == false) return false;
            //da li odg prvih par cifara 
            string pom = Convert.ToString(datumRodjenja);

            string pom2;

            if (pom[4] != '.')
            {

                if (pom[3] == '.' && pom[1] == '.')
                {

                    pom2 = "0" + Convert.ToString(pom[0]) + "0" + Convert.ToString(pom[2]) + Convert.ToString(pom[5]) + Convert.ToString(pom[6]) + Convert.ToString(pom[7]);

                }
                else if (pom[1] == '.' && pom[3] != '.')
                {

                    pom2 = "0" + Convert.ToString(pom[0]) + Convert.ToString(pom[2]) + Convert.ToString(pom[3]) + Convert.ToString(pom[6]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]);
                }
                else
                {

                    pom2 = Convert.ToString(pom[0]) + Convert.ToString(pom[1]) + Convert.ToString(pom[3]) +
                    Convert.ToString(pom[4]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]) + Convert.ToString(pom[9]);
                }


            }

            else
            {

                pom2 = Convert.ToString(pom[0]) + Convert.ToString(pom[1]) + "0" + Convert.ToString(pom[3]) +
                   Convert.ToString(pom[6]) + Convert.ToString(pom[7]) + Convert.ToString(pom[8]);
            }

            pom = Convert.ToString(JMBG[0]) + Convert.ToString(JMBG[1]) + Convert.ToString(JMBG[2]) + Convert.ToString(JMBG[3]) + Convert.ToString(JMBG[4]) + Convert.ToString(JMBG[5]) + Convert.ToString(JMBG[6]);

            if (pom != pom2) return false;

            string pom3 = JMBG;
            int cifra = Convert.ToInt32(pom3[pom3.Length - 1]) - 48;
            int suma = 11 - ((7 * (Convert.ToInt32(pom3[0]) + Convert.ToInt32(pom3[6]) - 96) + 6 * (Convert.ToInt32(pom3[1])
                 + Convert.ToInt32(pom3[7]) - 96) + 5 * (Convert.ToInt32(pom3[2]) + Convert.ToInt32(pom3[8]) - 96) +
                 4 * (Convert.ToInt32(pom3[3]) + Convert.ToInt32(pom3[9]) - 96) + 3 * (Convert.ToInt32(pom3[4]) +
                 Convert.ToInt32(pom3[10]) - 96) + 2 * (Convert.ToInt32(pom3[5]) + Convert.ToInt32(pom3[11]) - 96)) % 11);

            if (suma != cifra) return false;


            return true;

        }

    }
}
