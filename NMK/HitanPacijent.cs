﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;

namespace NMK
{
    public class HitanPacijent : Pacijent
    {
        private string prvaPomoc;

        public HitanPacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, DateTime datumPrijema, string prvaPomoc, string uzrokSmrti, DateTime vrijemeSmrti, DateTime vrijemeObdukcije, Karton pKarton, string user, string password) :
            base(ime, prezime, datumRodjenja, maticniBroj, spol, adresaStanovanja, bracnoStanje, uzrokSmrti, datumPrijema, vrijemeSmrti, vrijemeObdukcije, pKarton,user,password)
        {
            this.prvaPomoc = prvaPomoc;
        }

        public string PrvaPomoc
        {
            get
            {
                return prvaPomoc;
            }
            set
            {
                prvaPomoc = value;
            }
        }

        public string DajPrvuPomoc()
        {
            return prvaPomoc;
        }

        public HitanPacijent(string ppomoc)
        {
            prvaPomoc = ppomoc;
        }

        public HitanPacijent()
        {

        }

       /* public override Pacijent RegistrirajNovogPacijenta()
        {
            string pomDatRodj;
            string pomDatPrij;
            Console.WriteLine("Unesite detaljan opis prve pomoci: ");
            prvaPomoc = Console.ReadLine(); Console.WriteLine("Unesite ime: ");
            ime = Console.ReadLine();
            Console.WriteLine("Unesite prezime: ");
            prezime = Console.ReadLine();
            Console.WriteLine("Unesite datum rođenja: ");
            pomDatRodj = Console.ReadLine();
            DateTime.TryParse(pomDatRodj, out datumRodjenja);
            Console.WriteLine("Unesite matični broj: ");
            maticniBroj = Console.ReadLine();
            Console.WriteLine("Unesite spol: ");
            spol = Console.ReadLine();
            Console.WriteLine("Unesite adresu stanovanja: ");
            adresaStanovanja = Console.ReadLine();
            Console.WriteLine("Bračno stanje: ");
            bracnoStanje = Console.ReadLine();
            //Console.WriteLine("Unesite broj kartona: ");
            int preminuo;
            string pomPreminuo;
            Console.WriteLine("Da li je pacijent preminuo: \n" +
                "0. JESTE \n" +
                "1. NIJE ");
            pomPreminuo = Console.ReadLine();
            Int32.TryParse(pomPreminuo, out preminuo);
            if (preminuo==0)
            {

                string pomVrSmrti;
                string pomVrObduk;
                Console.WriteLine("Unesite uzrok smrti: ");
                uzrokSmrti = Console.ReadLine();
                Console.WriteLine("Vrijeme smrti: ");
                pomVrSmrti = Console.ReadLine();
                DateTime.TryParse(pomVrSmrti, out vrijemeSmrti);
                Console.WriteLine("Vrijeme obdukcije: ");
                pomVrObduk = Console.ReadLine();
                DateTime.TryParse(pomVrObduk, out vrijemeObdukcije);
            }
            else if(preminuo==1)
            {
                pKarton = new Karton(Console.ReadLine());
                pKarton.OtvoriNoviKarton();

                
                Console.WriteLine("Zakažite pregled za (unesite datum): ");
                pomDatPrij = Console.ReadLine();
                DateTime.TryParse(pomDatPrij, out datumPrijema);
                Ordinacije o = new Ordinacije();
                string pomBrOrd;
                int brOrd;
                Console.WriteLine("Koji pregled želite otkazati: \n" +
                "1. Dermatološki \n" +
                "2. Internistički \n" +
                "3. Kardiološki \n" +
                "4. Laboratorijski \n" +
                "5. Neurohirurški (ordinacija u pripremi) \n" +
                "6. Oftamološki \n" +
                "7. Opća hirurgija (ordinacija u pripremi) \n" +
                "8. Ortopedski \n" +
                "9. Otorinolaringološki \n" +
                "10.Plastična hirurgija (ordinacija u pripremi) \n" +
                "11. Stomatološki \n");
                pomBrOrd = Console.ReadLine();
                Int32.TryParse(pomBrOrd, out brOrd);
                o.ZakaziPregled(brOrd);
                Console.WriteLine("Pacijent je {0}. na listi čekanja za ovaj pregled", o.DajBrojLjudiNaCekanju(brOrd));
            }
            return this;
        }

        public override string IspisiPacijenta()
        {
            string ispis;
            ispis=("Ime i prezime: " + DajImeIPrezime().ToString() + "\n" +
                "Datum rođenja: " + DajDatumRodjenja().ToString() + "\n" +
                "Matični broj: " + DajMaticniBroj().ToString() + "\n" +
                "Broj kartona: " + pKarton.DajBrojKartona().ToString() + "\n" +
                "Spol: " + DajSpol().ToString() + "\n" +
                "Adresa stanovanja: " + DajAdresuStanovanja().ToString() + "\n" +
                "Bračno stanje: " + DajBracnoStanje().ToString() + "\n" +
                "Prva pomoc koja je pruzena: " + DajPrvuPomoc().ToString() + "\n" +
                "Datum prijema: " + DajDatumPrijema().ToString() + "\n" +
                "Vrijeme smrti: " + DajVrijemeSmrti().ToString() + "\n" +
                "Uzrok smrti: " + DajUzrokSmrti().ToString() + "\n" +
                "Vrijeme obdukcije: " + DajVrijemeObdukcije().ToString() + "\n");
            return ispis;
        }*/

       
    }
}
