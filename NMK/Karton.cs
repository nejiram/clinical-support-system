﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;

namespace NMK
{
    public class Karton: InterfaceIspisiKarton
    {
        private string brojKartona, terapija, anamneza, dijagnoza, alergije;
        

        public Karton(string brojKartona, string terapija, string anamneza, string dijagnoza, string alergije)
        {
            this.brojKartona = brojKartona;
            this.terapija = terapija;
            this.anamneza = anamneza;
            this.dijagnoza = dijagnoza;
            this.alergije = alergije;
        }

        public Karton(string brojKartona)
        {
            this.brojKartona = brojKartona;
        }

        public string BrojKartona
        {
            get
            {
                return brojKartona;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.BrojKartona()) throw new System.ArgumentException("Broj kartona moze sadrzavati samo pet znakova!");
                brojKartona = value;
            }
        }

        public string Terapija
        {
            get
            {
                return terapija;
            }
            set
            {
                terapija = value;
            }
        }

        public string Anamneza
        {
            get
            {
                return anamneza;
            }
            set
            {
                anamneza = value;
            }
        }

        public string Dijagnoza
        {
            get
            {
                return dijagnoza;
            }
            set
            {
                dijagnoza = value;
            }
        }

        public string Alergije
        {
            get
            {
                return alergije;
            }
            set
            {
                alergije = value;
            }
        }

        public string DajBrojKartona()
        {
            return brojKartona;
        }

        public string DajTerapiju()
        {
            return terapija;
        }

        public string DajAnamnezu()
        {
            return anamneza;
        }

        public string DajDijagnozu()
        {
            return dijagnoza;
        }

        public string DajAlergije()
        {
            return alergije;
        }

        public string IspisiKarton()
        {
            string ispis;
            ispis = ("Broj kartona: " + brojKartona.ToString() + "\n" +
                "Anamneza: " + "\n");

           
                ispis += (anamneza + "\n");

            ispis += ("Alergije: " + "\n");
            
                ispis += (alergije + "\n");
            ispis += ("Dijagnoza: " + "\n");
            
                ispis += (dijagnoza + "\n");
            ispis += ("Terapija: " + "\n");
            
                ispis += (terapija + "\n");

            return ispis;
        }

        public void AzurirajAnamnezu(string a)
        {
            anamneza = a;
        }

        public void AzurirajAlergije(string a)
        {
            alergije = a;
        }

        public void AzurirajDijagnozu(string a)
        {
            dijagnoza = a;
        }

        public void AzurirajTerapiju(string a)
        {
            terapija = a;
        }

        /*public void UpisiAnamnezu(string anam)
        {
            anamneza.Add(anam);
        }

        public void UpisiAlergije(string alerg)
        {
            alergije.Add(alerg);
        }

        public void UpisiDijagnozu(string dijag)
        {
            dijagnoza.Add(dijag);
        }

        public void UpisiTerapiju(string th)
        {
            terapija.Add(th);
        }

        public Karton OtvoriNoviKarton()
        {
            Console.WriteLine("Unesite broj kartona: ");
            brojKartona=Console.ReadLine();
            Console.WriteLine("Unesite anamnezu: ");
            anamneza.Add(Console.ReadLine());
            Console.WriteLine("Unesite alergije: ");
            alergije.Add(Console.ReadLine());
            Console.WriteLine("Unesite dijagnozu: ");
            dijagnoza.Add(Console.ReadLine());
            Console.WriteLine("Unesite terapiju: ");
            terapija.Add(Console.ReadLine());
            return this;
        }

        public void BrisiAnamnezu(int index)
        {
            for (int i = 0; i < anamneza.Count; i++)
            {
                if (index == i) anamneza.RemoveAt(i);
            }
        }

        public void BrisiAlergiju(int index)
        {
            for (int i = 0; i < alergije.Count; i++)
            {
                if (index == i) alergije.RemoveAt(i);
            }
        }

        public void BrisiDijagnozu(int index)
        {
            for (int i = 0; i < dijagnoza.Count; i++)
            {
                if (index == i) dijagnoza.RemoveAt(i);
            }
        }

        public void BrisiTerapiju(int index)
        {
            for(int i = 0; i < terapija.Count; i++)
            {
                if (index == i) terapija.RemoveAt(i);
            }
        }

        public void BrisiSve()
        {
            anamneza.Clear();
            alergije.Clear();
            dijagnoza.Clear();
            terapija.Clear();
        }

        public void UpisiSve(string anam, string alerg, string dijag, string th)
        {
            anamneza.Add(anam);
            alergije.Add(alerg);
            dijagnoza.Add(dijag);
            terapija.Add(th);
        }
        
        public string IspisiAnamnezu()
        {
            string ispis;
            ispis = ("Anamneza: " + "\n");
            for (int i = 0; i < anamneza.Count; i++)
                ispis += (anamneza[i] + "\n");
            return ispis;
        }

        public string IspisiAlergije()
        {
            string ispis;
            ispis = ("Alergije: " + "\n");
            for (int i = 0; i < alergije.Count; i++)
                ispis += (alergije[i] + "\n");
            return ispis;
        }

        public string IspisiDijagnozu()
        {
            string ispis;
            ispis = ("Dijagnoza: " + "\n");
            for (int i = 0; i < dijagnoza.Count; i++)
                ispis += (dijagnoza[i] + "\n");
            return ispis;
        }

        public string IspisiTerapiju()
        {
            string ispis;
            ispis = ("Terapija: " + "\n");
            for (int i = 0; i < terapija.Count; i++)
                ispis += (terapija[i] + "\n");
            return ispis;
        }*/
    }
}
