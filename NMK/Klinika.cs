﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Validacija;

namespace NMK
{
    public sealed class Klinika
    {
        private List<Doktor> doktori = new List<Doktor>();
        private Ordinacije o=new Ordinacije();
        private List<Pacijent> pacijenti = new List<Pacijent>();
        private List<MedicinskiTehnicar> medTehnicari = new List<MedicinskiTehnicar>();
        private List<Portir> portiri = new List<Portir>();

        public Klinika()
        {
            doktori.Add(new Doktor("Tanja", "Vrčić", 1, true, true, "TanjaV", "12345"));
            doktori.Add(new Doktor("Amra", "Džananović", 2, true, true,"AmraDz", "23456"));
            doktori.Add(new Doktor("Damir", "Macić", 3, true, true,"DamirM", "34567"));
            doktori.Add(new Doktor("Nedžad", "Rustempašić", 4, true, true, "NedzadR", "45678"));
            doktori.Add(new Doktor("Vladimir", "Kojović", 6, false, false, "VladimirK", "56789"));
            doktori.Add(new Doktor("Dragoje", "Starović", 8, false, true, "DragojeS", "54321"));
            doktori.Add(new Doktor("Safet", "Mujić", 9, false, false, "SafetM", "65432"));
            doktori.Add(new Doktor("Nusret", "Sarajlić", 11, true, true, "NusretS", "76543"));

            medTehnicari.Add(new MedicinskiTehnicar("Amila", "Hadzic", "AmilaH", "87654"));
            medTehnicari.Add(new MedicinskiTehnicar("Fikret", "Alihodzic", "FikretA", "98765"));
            medTehnicari.Add(new MedicinskiTehnicar("Šejla", "Delić", "SejlaD", "02468"));
            medTehnicari.Add(new MedicinskiTehnicar("Nermin", "Bilalović", "NerminB", "13579"));

            portiri.Add(new Portir("Hasan", "Hasanić", "HasanH", "86420"));
            portiri.Add(new Portir("Mujo", "Mujić", "MujoM", "97531"));

           pacijenti.Add(new Pacijent("Nejira", "Musić", new DateTime(1997,5,17), "1705997175031", "zensko", "Kolibe 35", "/", "",  new DateTime(2017,12,22),new Karton("12345"), "nejiram", "17832"));

        }
             
        public int BrojPacijenata()
        {
            return pacijenti.Count;
        }

        public List<Doktor> DajDoktore()
        {
            return doktori;
        }

        public List<MedicinskiTehnicar> DajMedTehnicare()
        {
            return medTehnicari;
        }

        public List<Portir> DajPortire()
        {
            return portiri;
        }

        public List<Pacijent> DajPacijente()
        {
            return pacijenti;
        }

        /*public void IspisiPacijenta(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].IspisiPacijenta();
                }
            }
        }
        */

        /*public void IspisiHitnogPacijenta(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].IspisiPacijenta();
                }
            }
        }
        */

        public void DodajPacijenta(Pacijent P)
        {
            pacijenti.Add(P);
        }

        public void DodajHitnogPacijenta(HitanPacijent p)
        {
            pacijenti.Add(p);
        }

        /*public void DodajPacijenta()
        {
            Pacijent p = new Pacijent();
            pacijenti.Add(p.RegistrirajNovogPacijenta());
        }*/

        /*public void DodajHitnogPacijenta()
        {
            HitanPacijent p = new HitanPacijent();
            pacijenti.Add(p.RegistrirajNovogPacijenta());
        }*/

        public void OdjaviPacijenta(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti.Remove(pacijenti[i]);
                }
            }
        }

        
        

        public bool PretragaPoJMBG(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    return true;
                }
            }
            return false;
        }

        public int IznosRacuna(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    return pacijenti[i].DajPlacanje();
                }
            }
            return 0;
        }

        public void ZakaziPregled(string jmbg, int brOrd)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].PovecajBrojPregleda();
                    o.ZakaziPregled(brOrd);
                }
            }
        }

        public void OtkaziPregled(string jmbg, int brOrd)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].SmanjiBrojPregleda();
                }
                o.OtkaziPregled(brOrd);
            }
        }

        public void DodajNaRacun(string jmbg, int cijena)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                   pacijenti[i].PovecajPlacanje(cijena);
                }
            }
        }

        public void SmanjiRacun(string jmbg, int cijena)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].SmanjiPlacanje(cijena);
                }
            }
        }

        public void IzvrsiNaplatu(string jmbg, int cijena)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    pacijenti[i].IzvrsiNaplatu(cijena);
                }
            }
        }

        public int DajBrojPregeldaP(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].DajMaticniBroj().Equals(jmbg))
                {
                    return pacijenti[i].DajBrojPregleda();
                }
            }
            return 0;
        }

        public int DajStanjeOrd(int brOrd)
        {
            return o.DajBrojLjudiNaCekanju(brOrd);
        }
    }
}
