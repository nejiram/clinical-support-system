﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;

namespace NMK
{
    public class Ordinacija
    {
        private int brojOrdinacije;
        private string naziv;
        private Doktor glavniDoktor;
        private Aparat aparati;
        private int ljudiNaCekanju;
        
        public Ordinacija()
        {
            ljudiNaCekanju = 0;
        }
        public Ordinacija(int brojOrdinacije, string naziv, Doktor glavniDoktor, Aparat aparati, int ljudiNaCekanju)
        {
            this.brojOrdinacije = brojOrdinacije;
            this.naziv = naziv;
            this.glavniDoktor = glavniDoktor;
            this.aparati = aparati;
            this.ljudiNaCekanju = ljudiNaCekanju;
        }

        public Ordinacija(int brojOrdinacije)
        {
            this.brojOrdinacije = brojOrdinacije;
            ljudiNaCekanju = 0;
        }

        public int BrojOrdinacije
        {
            get
            {
                return brojOrdinacije;
            }
            set
            {
                Validacija.validacija v = new Validacija.validacija();
                if (!v.BrOrdinacije()) throw new System.ArgumentException("Ordinacije su numerirane brojevima od 1 do 11!");
                brojOrdinacije = value;
            }
        }

        public string DajImeIPrezime()
        {
            return glavniDoktor.DajImeIPrezime();
        }

        public bool DaLiJeUOrdinaciji()
        {
            return glavniDoktor.DaLiJeUOrdinaciji();
        }

        public bool DaLiJeZauzet()
        {
            return glavniDoktor.DaLiJeZauzet();
        }

        public string DajNazivAparata()
        {
            return aparati.DajNazivAparata();
        }

        public bool DaLiRadi()
        {
            return aparati.DaLiRadi();
        }

        public int DajBrojLjudiNaCekanju()
        {
            return ljudiNaCekanju;
        }
      
        public void SkiniSaListeCekanja()
        {
            ljudiNaCekanju = ljudiNaCekanju - 1;
        }

        public void ZakaziPregled()
        {
            ljudiNaCekanju ++;
        }

        public int DajBrojOrdinacije()
        {
            return brojOrdinacije;
        }

        
    }
}
