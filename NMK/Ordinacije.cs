﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace NMK
{
    public class Ordinacije
    {
        private List<Ordinacija> listaOrd = new List<Ordinacija>();

        public Ordinacije()
        {
            listaOrd.Add(new Ordinacija(1, "DERMATOLOGIJA", new Doktor("Tanja","Vrčić",1,true,true,"TanjaV","12345"), new Aparat("CRYO DERM", 1, false), 1));
            listaOrd.Add(new Ordinacija(2, "INTERNA MEDICINA", new Doktor("Amra", "Džananović", 2, true, true,"AmraDz","23456"), new Aparat("EKG", 2, true), 2));
            listaOrd.Add(new Ordinacija(3, "KARDIOLOGIJA", new Doktor("Damir", "Macić", 3, true, true,"DamirM","34567"), new Aparat("HOLTER EKG", 3, true), 3));
            listaOrd.Add(new Ordinacija(4, "LABORATORIJA", new Doktor("Nedžad", "Rustempašić", 4, true, true,"NedzadR","45678"), new Aparat("LABORATORIJSKI FRIŽIDER", 4, false), 4));
            listaOrd.Add(new Ordinacija(6, "OFTAMOLOGIJA", new Doktor("Vladimir", "Kojović", 6, false, false,"VladimirK","56789"), new Aparat("MAMOGRAF", 6, true), 5));
            listaOrd.Add(new Ordinacija(8, "ORTOPEDIJA", new Doktor("Dragoje", "Starović", 8, false, true,"DragojeS","54321"), new Aparat(), 6));
            listaOrd.Add(new Ordinacija(9, "OTORINOLARINGOLOGIJA", new Doktor("Safet", "Mujić", 9, false, false,"SafetM","65432"), new Aparat(), 7));
            listaOrd.Add(new Ordinacija(11, "STOMATOLOGIJA", new Doktor("Nusret", "Sarajlić", 11, true, true,"NusretS","76543"), new Aparat("RENDGEN", 11, true),8));
        }

        /*public void IspisiStanje(int brojOrd)
        {
            for(int i = 0; i <listaOrd.Count; i++)
            {
                if(brojOrd==5 || brojOrd==7 || brojOrd == 10)
                {
                    Console.WriteLine("ORDINACIJA U PRIPREMI!");
                }
                else if (listaOrd[i].DajBrojOrdinacije()==brojOrd)
                {
                    Console.WriteLine("Glavni doktor: {0} ", listaOrd[i].DajImeIPrezime());
                    bool uOrd = listaOrd[i].DaLiJeUOrdinaciji();
                    if (uOrd) Console.WriteLine("Doktor je u ordinaciji");
                    else Console.WriteLine("Doktor nije u ordinaciji");
                    bool zauzet = listaOrd[i].DaLiJeZauzet();
                    if (zauzet) Console.WriteLine("Doktor je zauzet");
                    else Console.WriteLine("Doktor nije zauzet");
                    Console.WriteLine("Aparat: {0} ", listaOrd[i].DajNazivAparata());
                    bool apRadi = listaOrd[i].DaLiRadi();
                    if (apRadi) Console.WriteLine("Aparat radi");
                    else Console.WriteLine("Aparat ne radi");
                    Console.WriteLine("Broj ljudi koji čekaju ovaj pregled: {0} ", listaOrd[i].DajBrojLjudiNaCekanju());
                    Console.WriteLine();
                }
            }
        }*/

        public void OtkaziPregled (int brOrd)
        {
            for(int i=0; i < listaOrd.Count; i++)
            {
                if (listaOrd[i].DajBrojOrdinacije()==brOrd)
                {
                    listaOrd[i].SkiniSaListeCekanja();
                }
            }
        }

        public void ZakaziPregled(int brOrd)
        {
            for(int i=0; i < listaOrd.Count; i++)
            {
                if (listaOrd[i].DajBrojOrdinacije() == brOrd)
                {
                    Console.WriteLine("Zakazao sam mrtvi pregled");
                    listaOrd[i].ZakaziPregled();
                }
            }
        }

        public int DajBrojLjudiNaCekanju(int brOrd)
        {
            for (int i = 0; i < listaOrd.Count; i++)
            {
                if (listaOrd[i].DajBrojOrdinacije() == brOrd)
                {
                    return listaOrd[i].DajBrojLjudiNaCekanju();
                }
            }
            return 0;
        }

        
         
    } 
}
