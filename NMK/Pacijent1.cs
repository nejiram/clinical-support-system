﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;


namespace NMK
{
    public partial class Pacijent
    {
        protected string ime, prezime, maticniBroj, spol, adresaStanovanja, bracnoStanje, uzrokSmrti, user,password;
        protected DateTime datumRodjenja, datumPrijema, vrijemeSmrti, vrijemeObdukcije;
        protected Karton pKarton;
        protected int uReduCekanja;
        protected int plati,brPregleda;

        public Pacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, string uzrokSmrti, DateTime datumPrijema, DateTime vrijemeSmrti, DateTime vrijemeObdukcije, Karton pKarton, string user, string password)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.maticniBroj = maticniBroj;
            this.spol = spol;
            this.adresaStanovanja = adresaStanovanja;
            this.bracnoStanje = bracnoStanje;
            this.uzrokSmrti = uzrokSmrti;
            this.datumPrijema = datumPrijema;
            this.vrijemeSmrti = vrijemeSmrti;
            this.vrijemeObdukcije = vrijemeObdukcije;
            this.pKarton = pKarton;
            this.user = user;
            this.password = password;
        }

        public Pacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, string uzrokSmrti, DateTime datumPrijema, Karton pKarton,  string user, string password)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.maticniBroj = maticniBroj;
            this.spol = spol;
            this.adresaStanovanja = adresaStanovanja;
            this.bracnoStanje = bracnoStanje;
            this.uzrokSmrti = uzrokSmrti;
            this.datumPrijema = datumPrijema;
            this.pKarton = pKarton;
            this.user = user;
            this.password = password;
        }

        public Pacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, DateTime datumPrijema, Karton pKarton, string user, string password)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.maticniBroj = maticniBroj;
            this.spol = spol;
            this.adresaStanovanja = adresaStanovanja;
            this.bracnoStanje = bracnoStanje;
            this.datumPrijema = datumPrijema;
            this.pKarton = pKarton;
            this.user = user;
            this.password = password;
            this.datumPrijema = datumPrijema;
            PovecajBrojPregleda();
        }

        public string DajBrojKartona()
        {
            return pKarton.DajBrojKartona();
        }

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Ime()) throw new System.ArgumentException("Ime smije sadržavati samo slova!");
                ime = value;
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Prezime()) throw new System.ArgumentException("Prezime smije sadržavati samo slova!");
                prezime = value;
            }
        }

        public DateTime DatumRodjenja
        {
            get
            {
                return datumRodjenja;
            }
            set
            {
                datumRodjenja = value;
            }
        }

        public string MaticniBroj
        {
            get
            {
                return maticniBroj;
            }
            set
            {
                Validacija.validacija v = new Validacija.validacija();
                if (!v.MaticniBroj()) throw new System.ArgumentException("Maticni broj moze sadrzavati samo brojeve!");
                maticniBroj = value;
            }
        }

        public string Spol
        {
            get
            {
                return spol;
            }
            set
            {
                spol = value;
            }
        }

        public string AdresaStanovanja
        {
            get
            {
                return adresaStanovanja;
            }
            set
            {
                adresaStanovanja = value;
            }
        }

        public string BracnoStanje
        {
            get
            {
                return bracnoStanje;
            }
            set
            {
                bracnoStanje = value;
            }
        }

        public string UzrokSmrti
        {
            get
            {
                return uzrokSmrti;
            }
            set
            {
                uzrokSmrti = value;
            }
        }

        public DateTime DatumPrijema
        {
            get
            {
                return datumPrijema;
            }
            set
            {
                datumPrijema = value;
            }
        }

        public DateTime VrijemeSmrti
        {
            get
            {
                return vrijemeSmrti;
            }
            set
            {
                vrijemeSmrti = value;
            }
        }

        public DateTime VrijemeObdukcije
        {
            get
            {
                return vrijemeObdukcije;
            }
            set
            {
                vrijemeObdukcije = value;
            }
        }

        public Karton PKarton
        {
            get
            {
                return pKarton;
            }
            set
            {
                pKarton = value;
            }
        }
         
        

        public string DajAnamnezu()
        {
            return pKarton.DajAnamnezu();
        }

        public string DajAlergije()
        {
            return pKarton.DajAlergije();
        }

        public string DajDijagnozu()
        {
            return pKarton.DajDijagnozu();
        }

        public string DajTerapiju()
        {
            return pKarton.DajTerapiju();
        }

        public void AzurirajAnamnezu(string a)
        {
            PKarton.AzurirajAnamnezu(a);
        }

        public void AzurirajAlergije(string a)
        {
            PKarton.AzurirajAlergije(a);
        }

        public void AzurirajDijagnozu(string a)
        {
            PKarton.AzurirajDijagnozu(a);
        }

        public void AzurirajTerapiju(string a)
        {
            PKarton.AzurirajTerapiju(a);
        }

        /* public virtual string IspisiPacijenta()
         {
             string ispis;
             ispis = ("Ime i prezime: " + DajImeIPrezime().ToString() + "\n" +
                 "Datum rođenja: " + DajDatumRodjenja().ToString() + "\n" +
                 "Matični broj: " + DajMaticniBroj().ToString() + "\n" +
                 "Broj kartona: " + pKarton.DajBrojKartona().ToString() + "\n" +
                 "Spol: " + DajSpol().ToString() + "\n" +
                 "Adresa stanovanja: " + DajAdresuStanovanja().ToString() + "\n" +
                 "Bračno stanje: " + DajBracnoStanje().ToString() + "\n" +
                 "Datum prijema: " + DajDatumPrijema().ToString() + "\n" +
                 "Vrijeme smrti: " + DajVrijemeSmrti().ToString() + "\n" +
                 "Uzrok smrti: " + DajUzrokSmrti().ToString() + "\n" +
                 "Vrijeme obdukcije: " + DajVrijemeObdukcije().ToString() + "\n"  );
             return ispis;
         }*/

        /*public virtual Pacijent RegistrirajNovogPacijenta()
        {
            string pomDatRodj;
            string pomDatPrij;
            Console.WriteLine("Unesite ime: ");
            ime = Console.ReadLine();
            Console.WriteLine("Unesite prezime: ");
            prezime = Console.ReadLine();
            Console.WriteLine("Unesite datum rođenja: ");
            pomDatRodj = Console.ReadLine();
            DateTime.TryParse(pomDatRodj, out datumRodjenja);
            Console.WriteLine("Unesite matični broj: ");
            maticniBroj = Console.ReadLine();
            Console.WriteLine("Unesite spol: ");
            spol = Console.ReadLine();
            Console.WriteLine("Unesite adresu stanovanja: ");
            adresaStanovanja = Console.ReadLine();
            Console.WriteLine("Bračno stanje: ");
            bracnoStanje = Console.ReadLine();
            //Console.WriteLine("Unesite broj kartona: ");
            int preminuo;
            string pomPreminuo;
            Console.WriteLine("Da li je pacijent preminuo: \n" +
                "0. NIJE \n" +
                "1. JESTE \n");
            pomPreminuo = Console.ReadLine();
            Int32.TryParse(pomPreminuo, out preminuo);
            if (preminuo==1)
            {
                string pomVrSmrti;
                string pomVrObduk;
                Console.WriteLine("Unesite uzrok smrti: ");
                uzrokSmrti = Console.ReadLine();
                Console.WriteLine("Vrijeme smrti: ");
                pomVrSmrti = Console.ReadLine();
                DateTime.TryParse(pomVrSmrti, out vrijemeSmrti);
                Console.WriteLine("Vrijeme obdukcije: ");
                pomVrObduk = Console.ReadLine();
                DateTime.TryParse(pomVrObduk, out vrijemeObdukcije);
            }
            else if(preminuo==0)
            {
                pKarton = new Karton(Console.ReadLine());
                pKarton.OtvoriNoviKarton();
                
                Console.WriteLine("Zakažite pregled za (unesite datum): ");
                pomDatPrij = Console.ReadLine();
                DateTime.TryParse(pomDatPrij, out datumPrijema);
                Ordinacije o=new Ordinacije();
                string pomBrOrd;
                int brOrd;
                Console.WriteLine("Koji pregled želite otkazati: \n" +
                "1. Dermatološki \n" +
                "2. Internistički \n" +
                "3. Kardiološki \n" +
                "4. Laboratorijski \n" +
                "5. Neurohirurški (ordinacija u pripremi) \n" +
                "6. Oftamološki \n" +
                "7. Opća hirurgija (ordinacija u pripremi) \n" +
                "8. Ortopedski \n" +
                "9. Otorinolaringološki \n" +
                "10.Plastična hirurgija (ordinacija u pripremi) \n" +
                "11. Stomatološki \n");
                pomBrOrd = Console.ReadLine();
                Int32.TryParse(pomBrOrd, out brOrd);
                o.ZakaziPregled(brOrd);
                Console.WriteLine("Pacijent je {0}. na listi čekanja za ovaj pregled", o.DajBrojLjudiNaCekanju(brOrd));
            }
            return this;
        }*/

        public Pacijent() { }

        public int ZaPlatiti()
        {
            return plati;
        }

        
        public int KojiJeUReduCekanja()
        {
            return uReduCekanja;
        }
        
        public void PovecajPlacanje(int cijena)
        {
            plati += cijena;
        }

        public void SmanjiPlacanje(int cijena)
        {
            plati -= cijena;
        }

        public void IzvrsiNaplatu(int cijena)
        {
            plati = -cijena;
        }

        public void PovecajBrojPregleda()
        {
            brPregleda++;
        }

        public void SmanjiBrojPregleda()
        {
            brPregleda--;
        }

        public string User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string password)
        {

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            
            return sBuilder.ToString();
        }

        /*static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GetMd5Hash(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }*/
    }
}
