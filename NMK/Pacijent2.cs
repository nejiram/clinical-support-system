﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;
using System.Security.Cryptography;

namespace NMK
{
    public partial class Pacijent
    {
        public string DajImeIPrezime()
        {
            return ime + " " + prezime;
        }

        public DateTime DajDatumRodjenja()
        {
            return datumRodjenja;
        }

        public string DajMaticniBroj()
        {
            return maticniBroj;
        }

        public string DajSpol()
        {
            return spol;
        }

        public string DajAdresuStanovanja()
        {
            return adresaStanovanja;
        }

        public string DajBracnoStanje()
        {
            return bracnoStanje;
        }

        public DateTime DajDatumPrijema()
        {
            return datumPrijema;
        }

        public DateTime DajVrijemeSmrti()
        {
            return vrijemeSmrti;
        }

        public DateTime DajVrijemeObdukcije()
        {
            return vrijemeObdukcije;
        }

        public string DajUzrokSmrti()
        {
            return uzrokSmrti;
        }

        public int DajPlacanje()
        {
            return plati;
        }

        public int DajBrojPregleda()
        {
            return brPregleda;
        }
    }
}
