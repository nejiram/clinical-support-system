﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMK
{
    class HitanPacijent: Pacijent
    {
        private string prvaPomoc;

        public HitanPacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, DateTime datumPrijema, string prvaPomoc, string uzrokSmrti, DateTime vrijemeSmrti, DateTime vrijemeObdukcije, Karton pKarton):
            base(ime, prezime, datumRodjenja, maticniBroj, spol, adresaStanovanja, bracnoStanje, uzrokSmrti, datumPrijema, vrijemeSmrti, vrijemeObdukcije, pKarton)
        {
            this.prvaPomoc = prvaPomoc;
        }

        public string PrvaPomoc
        {
            get
            {
                return prvaPomoc;
            }
            set
            {
                prvaPomoc = value;
            }
        }

    }
}
