﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validacija;

namespace NMK
{
    public abstract class Pacijent
    {
        private string ime, prezime, maticniBroj, spol, adresaStanovanja, bracnoStanje, uzrokSmrti;
        private DateTime datumRodjenja, datumPrijema, vrijemeSmrti, vrijemeObdukcije;
        private Karton pKarton ;

        public Pacijent(string ime, string prezime, DateTime datumRodjenja, string maticniBroj, string spol, string adresaStanovanja, string bracnoStanje, string uzrokSmrti, DateTime datumPrijema, DateTime vrijemeSmrti, DateTime vrijemeObdukcije, Karton pKarton)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.maticniBroj = maticniBroj;
            this.spol = spol;
            this.adresaStanovanja = adresaStanovanja;
            this.bracnoStanje = bracnoStanje;
            this.uzrokSmrti = uzrokSmrti;
            this.datumPrijema = datumPrijema;
            this.vrijemeSmrti = vrijemeSmrti;
            this.vrijemeObdukcije = vrijemeObdukcije;
            this.pKarton = pKarton;
        }

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Ime()) throw new System.ArgumentException("Ime smije sadržavati samo slova!");
                ime = value;
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                Validacija.validacija v = new validacija();
                if (!v.Prezime()) throw new System.ArgumentException("Prezime smije sadržavati samo slova!");
                prezime = value;
            }
        }

        public DateTime DatumRodjenja
        {
            get
            {
                return datumRodjenja;
            }
            set
            {
                datumRodjenja = value;
            }
        }

        public string MaticniBroj
        {
            get
            {
                return maticniBroj;
            }
            set
            {
                Validacija.validacija v = new Validacija.validacija();
                if (!v.MaticniBroj()) throw new System.ArgumentException("Maticni broj moze sadrzavati samo brojeve!");
                maticniBroj = value;
            }
        }

        public string Spol
        {
            get
            {
                return spol;
            }
            set
            {
                spol = value;
            }
        }

        public string AdresaStanovanja
        {
            get
            {
                return adresaStanovanja;
            }
            set
            {
                adresaStanovanja = value;
            }
        }

        public string BracnoStanje
        {
            get
            {
                return bracnoStanje;
            }
            set
            {
                bracnoStanje = value;
            }
        }

        public string UzrokSmrti
        {
            get
            {
                return uzrokSmrti;
            }
            set
            {
                uzrokSmrti = value;
            }
        }

        public DateTime DatumPrijema
        {
            get
            {
                return datumPrijema;
            }
            set
            {
                datumPrijema = value;
            }
        }

        public DateTime VrijemeSmrti
        {
            get
            {
                return vrijemeSmrti;
            }
            set
            {
                vrijemeSmrti = value;
            }
        }

        public DateTime VrijemeObdukcije
        {
            get
            {
                return vrijemeObdukcije;
            }
            set
            {
                vrijemeObdukcije = value;
            }
        }

        public Karton PKarton
        {
            get
            {
                return pKarton;
            }
            set
            {
                pKarton = value;
            }
        }
    }
}
